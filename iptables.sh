#!/bin/bash
I_WAN="eth0"
I_DMZ="eth1"
I_LAN="eth2"
A_WAN="10.3.4.16"
N_LAN="192.168.106.254/24"
N_DMZ="172.20.106.254/24"
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

iptables -F
iptables -t nat -F

# FW > WAN

iptables -t filter -A OUTPUT -p tcp --dport 80 -o $I_WAN -j ACCEPT;
iptables -t filter -A OUTPUT -p tcp --dport 443 -o $I_WAN -j ACCEPT;
iptables -t filter -A INPUT -p tcp --dport 22 -i $I_WAN -j ACCEPT;
iptables -t filter -A OUTPUT -p icmp -o $I_WAN -j ACCEPT;
iptables -t filter -A OUTPUT -p udp --dport 53 -o $I_WAN -j ACCEPT;

# FW > DMZ

iptables -t filter -A OUTPUT -p tcp --dport 80 -o $I_DMZ -j ACCEPT;
iptables -t filter -A OUTPUT -p tcp --dport 443 -o $I_DMZ -j ACCEPT;
iptables -t filter -A OUTPUT -p tcp --dport 22 -o $I_DMZ -j ACCEPT;
iptables -t filter -A OUTPUT -p icmp -o $I_DMZ -j ACCEPT;
iptables -t filter -A OUTPUT -p udp --dport 53 -o $I_DMZ -j ACCEPT;

# DMZ > WAN

iptables -t filter -A FORWARD -p tcp --dport 80 -i $I_DMZ -o $I_WAN -j ACCEPT;
iptables -t filter -A FORWARD -p tcp --dport 443 -i $I_DMZ -o $I_WAN -j ACCEPT;
iptables -t filter -A FORWARD -p tcp --dport 22 -i $I_WAN -o $I_DMZ -j ACCEPT;
iptables -t filter -A FORWARD -p icmp -i $I_DMZ -o $I_WAN -j ACCEPT;
iptables -t filter -A FORWARD -p udp --dport 53 -i $I_DMZ -o $I_WAN -j ACCEPT;

# WAN > DMZ

iptables -t filter -A FORWARD -p tcp --dport 80 -i $I_WAN -o $I_DMZ -j ACCEPT;
iptables -t filter -A FORWARD -p tcp --dport 443 -i $I_WAN -o $I_DMZ -j ACCEPT;
iptables -t filter -A FORWARD -p udp --dport 53 -i $I_WAN -o $I_DMZ -j ACCEPT;

# LAN > WAN

iptables -t filter -A FORWARD -s $N_LAN -i $I_LAN -j ACCEPT;


#conntrack

iptables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT;
iptables -t filter -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT;
iptables -t filter -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT;

# NAT

iptables -t nat -A POSTROUTING -o $I_WAN -j SNAT --to $A_WAN;

# PORTFORWARDING

iptables -t nat -A PREROUTING -p tcp --dport 80 -j DNAT --to 172.20.106.80:80;
iptables -t nat -A PREROUTING -p tcp --dport 443 -j DNAT --to 172.20.106.80:443;
iptables -t nat -A PREROUTING -p udp --dport 53 -j DNAT --to 172.20.106.53:53;
